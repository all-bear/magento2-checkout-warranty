<?php

namespace ITS\Warranty\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XPATH_WARRANTY_ENABLED        = 'its_warranty/settings/enabled';
    const XPATH_WARRANTY_PRODUCT_ID     = 'its_warranty/settings/product_id';
    const XPATH_WARRANTY_PRODUCT_QTY    = 'its_warranty/settings/product_qty';
    const XPATH_WARRANTY_DEFAULT_STATUS = 'its_warranty/settings/default_status';

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * @var \Magento\Store\Api\Data\StoreInterface
     */
    protected $store;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Store\Api\Data\StoreInterface $store
    ) {
        $this->productRepository = $productRepository;
        $this->store = $store;

        parent::__construct($context);
    }

    public function isEnabled()
    {
        return $this->store->getConfig(static::XPATH_WARRANTY_ENABLED) && $this->getWarrantyProductId();
    }

    public function getWarrantyProductId()
    {
        return intval($this->store->getConfig(static::XPATH_WARRANTY_PRODUCT_ID));
    }

    public function getDefaultStatus()
    {
        return intval($this->store->getConfig(static::XPATH_WARRANTY_DEFAULT_STATUS));
    }

    public function getWarrantyProductQty()
    {
        return intval($this->store->getConfig(static::XPATH_WARRANTY_PRODUCT_QTY));
    }

    public function getWarrantyProduct()
    {
        return $this->productRepository->getById($this->getWarrantyProductId());
    }
}