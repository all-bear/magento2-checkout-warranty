<?php

namespace ITS\Warranty\Observer;

use Magento\Framework\Event\ObserverInterface;

class CheckoutSubmitBeforeObserver implements ObserverInterface
{
    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    /**
     * @var \ITS\Warranty\Helper\Data
     */
    protected $moduleHelper;

    /**
     * @var \ITS\Warranty\Model\WarrantyStatusManagement
     */
    protected $warrantyStatusManagement;

    /**
     * @var \Magento\Quote\Model\QuoteValidator
     */
    protected $quoteValidator;

    public function __construct(
        \Magento\Checkout\Model\Cart $cart,
        \ITS\Warranty\Helper\Data $moduleHelper,
        \ITS\Warranty\Model\WarrantyStatusManagement $warrantyStatusManagement,
        \Magento\Quote\Model\QuoteValidator $quoteValidator
    ) {
        $this->cart                     = $cart;
        $this->moduleHelper             = $moduleHelper;
        $this->warrantyStatusManagement = $warrantyStatusManagement;
        $this->quoteValidator           = $quoteValidator;
    }

    /**
     * Add warranty product to quote
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->warrantyStatusManagement->getStatus()) {
            return $this;
        }

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getQuote();

        $this->quoteValidator->validateBeforeSubmit($quote);

        $product = $this->moduleHelper->getWarrantyProduct();
        $product->load($product->getId());

        $this->cart->addProduct(
            $this->moduleHelper->getWarrantyProduct(), [
                'qty' => $this->moduleHelper->getWarrantyProductQty()
            ]
        );

        // clear items cache, because we added warranty item to cart
        foreach ($quote->getAllAddresses() as $address) {
            $address->offsetUnset('cached_items_all');
        }

        $observer->getQuote()
            ->setTotalsCollectedFlag(false)
            ->collectTotals()
            ->save();

        $this->warrantyStatusManagement->handleWarrantyStatus(null);

        return $this;
    }
}
