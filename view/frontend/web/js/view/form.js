define(
    [
        'jquery',
        'underscore',
        'Magento_Ui/js/form/form',
        'ko',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils',
        'mage/storage'
    ],
    function ($,
              _,
              Component,
              ko,
              fullScreenLoader,
              quote,
              priceUtils,
              storage) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'ITS_Warranty/form'
            },

            isWarrantyAdded: ko.observable(window.checkoutConfig.warranty && window.checkoutConfig.warranty.status > 0),

            isTotalsDirty: false,

            initialize: function () {
                if (!this.isEnabled()) {
                    return this;
                }

                var self = this,
                    status = this.getData('status');

                this._super();

                this.isWarrantyAdded.subscribe(function (status) {
                    if (status) {
                        self.addWarranty();
                    } else {
                        self.removeWarranty();
                    }
                });

                quote.totals.subscribe(function () {
                    if (self.isTotalsDirty) {
                        self.isTotalsDirty = false;
                        return;
                    }

                    if (self.isWarrantyAdded()) {
                        self.addWarranty();
                    }
                });

                if (status) {
                    this.addWarranty(true);
                }

                return this;
            },

            getFormattedPrice: function (price) {
                return priceUtils.formatPrice(price, quote.getPriceFormat());
            },

            getConfig: function () {
                return window.checkoutConfig.warranty;
            },

            isEnabled: function () {
                return !!this.getConfig();
            },

            getData: function (key) {
                return this.getConfig()[key];
            },

            getWarrantyPrice: function () {
                return parseFloat(this.getData('price'))
            },

            getTaxPercentage: function () {
                var tax = 0;

                quote.totals().total_segments.forEach(function (segment) {
                    if (segment.code != 'tax') {
                        return;
                    }
                    var details = segment.extension_attributes.tax_grandtotal_details[0];

                    if (details) {
                        tax = details.rates[0].percent
                    }
                });


                return tax;
            },

            //TODO remove parseFloat (something wrong with base_grand_total - it's string)
            updateTotals: function (totals, totalIncrement) {
                var tmpTotals = totals,
                    subtotal = _.reduce(tmpTotals.items, function (memo, item) {
                        return memo + parseFloat(item.row_total);
                    }, 0);
                //tmpTotals.base_grand_total = parseFloat(tmpTotals.base_grand_total) + increment;
                //tmpTotals.grand_total = parseFloat(tmpTotals.grand_total) + increment;

                tmpTotals.base_subtotal = subtotal + totalIncrement;
                tmpTotals.subtotal = subtotal + totalIncrement;
                tmpTotals.tax_amount = parseFloat(tmpTotals.base_subtotal) * this.getTaxPercentage() / 100;

                for (var i in tmpTotals.total_segments) {
                    var total = tmpTotals.total_segments[i];

                    if (['tax'].indexOf(total.code) !== -1) {
                        total.value = parseFloat(tmpTotals['tax_amount']);
                    }

                    if (['grand_total'].indexOf(total.code) !== -1) {
                        total.value = parseFloat(tmpTotals[total.code]) + totalIncrement;

                        if (total.code == 'grand_total') {
                            total.value += parseFloat(tmpTotals.tax_amount);
                        }
                    }
                }

                return tmpTotals;
            },

            getUrl: function (urlTemplate, params) {
                _.each(params, function (val, key) {
                    urlTemplate = urlTemplate.replace(new RegExp('\\:' + key, 'g'), encodeURIComponent(val));
                });

                return urlTemplate;
            },

            getStatusActionUrl: function (status) {
                return this.getUrl(this.getData('status_api_url'), {
                    status: status
                });
            },

            changeWarrantyStatus: function (status, skipSessionUpdate) {
                if (skipSessionUpdate) {
                    return {
                        done: function (cb) {
                            cb.call();
                        }
                    }
                }

                fullScreenLoader.startLoader();

                return storage.post(
                    this.getStatusActionUrl(status)
                ).done(function () {
                    fullScreenLoader.stopLoader()
                });
            },

            removeWarranty: function (skipSessionUpdate) {
                var self = this;

                this.changeWarrantyStatus(0, skipSessionUpdate).done(function () {
                    var newTotals = self.updateTotals(quote.totals(), 0);

                    quote.totals(newTotals);
                });
            },

            addWarranty: function (skipSessionUpdate) {
                var self = this;

                return this.changeWarrantyStatus(1, skipSessionUpdate).done(function () {
                    var newTotals = self.updateTotals(quote.totals(), self.getWarrantyPrice());

                    self.isTotalsDirty = true;
                    quote.totals(newTotals);
                });
            }
        });
    }
);
