<?php

namespace ITS\Warranty\Api;

/**
 * Interface for managing warranty status information
 * @api
 */
interface WarrantyStatusManagementInterface
{
    /**
     * @param int $status
     *
     * @return array
     */
    public function handleWarrantyStatus($status);
}