# Warranty
Magento 2 module which gives you ability to create product for warranty and customer will be able to add this warranty product during checkout.

## Settings
Stores -> Configuration -> ITS Extensions -> Warranty.

- Warranty product id - id of warranty product. It data will be used for description and prices.
- Warranty product qty - qty of warranty product. It will be added to order with this qty.
- Is warranty added by default on checkout - default value for radio button "Add warranty".

## How to add it on checkout page
Add component 'ITS_Warranty/js/view/form' to your jsLayout. For example (layout xml file):
```javascript
<?xml version="1.0"?>

<page xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" layout="1column" xsi:noNamespaceSchemaLocation="urn:magento:framework:View/Layout/etc/page_configuration.xsd">
    <body>
        <referenceBlock name="checkout.root">
            <arguments>
                <argument name="jsLayout" xsi:type="array">
                    <item name="components" xsi:type="array">
                        <item name="checkout" xsi:type="array">
                            <item name="children" xsi:type="array">
                                <item name="sidebar" xsi:type="array">
                                    <item name="children" xsi:type="array">
                                        <item name="summary" xsi:type="array">
                                            <item name="children" xsi:type="array">
                                                <item name="add-warranty" xsi:type="array">
                                                    <item name="component" xsi:type="string">ITS_Warranty/js/view/form</item>
                                                </item>
                                            </item>
                                        </item>
                                    </item>
                                </item>
                            </item>
                        </item>
                    </item>
                </argument>
            </arguments>
        </referenceBlock>
    </body>
</page>
```