<?php

namespace ITS\Warranty\Model;

class WarrantyStatusManagement implements \ITS\Warranty\Api\WarrantyStatusManagementInterface
{
    const WARRANTY_STATUS_SESSION_KEY = 'warranty';

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $session;

    /**
     * @var \ITS\Warranty\Helper\Data
     */
    protected $moduleHelper;

    public function __construct(
        \Magento\Checkout\Model\Session $session,
        \ITS\Warranty\Helper\Data $moduleHelper
    ) {
        $this->session      = $session;
        $this->moduleHelper = $moduleHelper;
    }

    public function getStatus()
    {
        $status = $this->session->getData(self::WARRANTY_STATUS_SESSION_KEY);

        if (is_null($status)) {
            $status = $this->moduleHelper->getDefaultStatus();
        }

        return $status;
    }

    /**
     * @inheritdoc
     */
    public function handleWarrantyStatus($status)
    {
        $this->session->setData(self::WARRANTY_STATUS_SESSION_KEY, $status);
    }
}