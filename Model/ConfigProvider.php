<?php

namespace ITS\Warranty\Model;

class ConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    /** @var \Magento\Framework\View\Element\FormKey */
    protected $formKey;

    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $storeManager;

    /** @var WarrantyStatusManagement */
    protected $statusManagement;

    /**
     * @var \ITS\Warranty\Helper\Data
     */
    protected $moduleHelper;

    public function __construct(
        \Magento\Framework\View\Element\FormKey $formKey,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \ITS\Warranty\Model\WarrantyStatusManagement $statusManagement,
        \Magento\Store\Api\Data\StoreInterface $store,
        \ITS\Warranty\Helper\Data $moduleHelper
    ) {
        $this->formKey          = $formKey;
        $this->storeManager     = $storeManager;
        $this->statusManagement = $statusManagement;
        $this->moduleHelper     = $moduleHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        if (!$this->moduleHelper->isEnabled()) {
            return [];
        }

        $storeCode = $this->storeManager->getStore()->getCode();
        $product   = $this->moduleHelper->getWarrantyProduct();

        $config = [
            'warranty' => [
                'description'    => $product->getDescription(),
                'oldPrice'       => $product->getPrice() > $product->getFinalPrice() ? $product->getPrice() : null,
                'price'          => $product->getFinalPrice(),
                'status_api_url' => "/rest/$storeCode/V1/warranty/status/:status",
                'status'         => $this->statusManagement->getStatus()
            ]
        ];

        return $config;
    }
}